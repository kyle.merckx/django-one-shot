# Generated by Django 4.1.3 on 2022-12-01 23:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "todos",
            "0003_remove_todolist_creaton_on_todolist_created_on_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="due_date",
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name="todoitem",
            name="task",
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name="todolist",
            name="created_on",
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
